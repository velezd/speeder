cd /home/zbeny/bin/speeder/
case $(ls /usr/bin | grep -m 1 -E "beesu|gksudo") in
	"gksudo")
		gksudo ./main.py >> speeder.log
		;;
	"beesu")
		beesu ./main.py >> speeder.log
		;;
	*)
		echo "You must have beesu or gksudo"
		;;
esac

