#!/usr/bin/python

# NOTE: This is probably not the best way how to do it, but I wanted to have
# working app as soon as possible so I did first thing that came to mind

import gtk
import gtk.glade
import glib
import os

# enable or disable manual PWM
def enable_pwm(state, path):
    if path != '':
        if os.path.exists(path + '_enable'):
            try:
                os.popen('echo ' + state + ' > ' + path + '_enable')
            except:
                exit('ERROR: Can\'t enable/disable PWM - ' + path)
        else:
            exit('ERROR: Device ' + path + ' doesn\'t exist')

# play alarm sound
def run_alarm():
    os.popen('aplay lib/alarm.wav')

class main:
    def __init__(self):

        # load config file
        self.config = {}
        execfile('./speeder.conf', self.config)

        # set initial fan speed
        self.spd1 = 50
        self.spd2 = 50
        self.spd3 = 50
        self.spd4 = 50

        # load GUI
        self.gladefile = 'lib/speeder.glade'
        self.wTree = gtk.glade.XML(self.gladefile, 'program')

        # assign buttons to functions
        dic = {'on_speed_1_value_changed': self.spd_1_changed,
               'on_speed_2_value_changed': self.spd_2_changed,
               'on_speed_3_value_changed': self.spd_3_changed,
               'on_speed_4_value_changed': self.spd_4_changed,
               'on_mod_1_clicked': self.mod_1,
               'on_mod_2_clicked': self.mod_2,
               'on_mod_3_clicked': self.mod_3,
               'on_mod_4_clicked': self.mod_4,
               #'on_btn_config_clicked': self.open_config,
               'on_program_destroy': self.kill}

        self.wTree.signal_autoconnect(dic)

        # set GUI labels
        label = self.wTree.get_widget('name_1')
        label.set_text(self.config['fan_1_name'])
        label = self.wTree.get_widget('name_2')
        label.set_text(self.config['fan_2_name'])
        label = self.wTree.get_widget('name_3')
        label.set_text(self.config['fan_3_name'])
        label = self.wTree.get_widget('name_4')
        label.set_text(self.config['fan_4_name'])
        label = self.wTree.get_widget('temp_name_1')
        label.set_text(self.config['temp_1_name'])
        label = self.wTree.get_widget('temp_name_2')
        label.set_text(self.config['temp_2_name'])
        label = self.wTree.get_widget('temp_name_3')
        label.set_text(self.config['temp_3_name'])
        label = self.wTree.get_widget('temp_name_4')
        label.set_text(self.config['temp_4_name'])
        label = self.wTree.get_widget('temp_name_5')
        label.set_text(self.config['temp_5_name'])

        # start "deamon"
        self.show(None)

        # enable manual fan speed
        enable_pwm('1', self.config['fan_1'])
        enable_pwm('1', self.config['fan_2'])
        enable_pwm('1', self.config['fan_3'])
        enable_pwm('1', self.config['fan_4'])

    ### Mods ###
    def mod_1(self, widget):
        self.mod_set(self.config['mod_1'])
    def mod_2(self, widget):
        self.mod_set(self.config['mod_2'])
    def mod_3(self, widget):
        self.mod_set(self.config['mod_3'])
    def mod_4(self, widget):
        self.mod_set(self.config['mod_4'])
        
    def mod_set(self, conf):
        conf = conf.split(',')
        if self.config['fan_1'] != '':
            self.spd1 = self.change_speed(int(conf[0]), self.spd1, self.config['fan_1'])
            spinbutton = self.wTree.get_widget('speed_1')
            spinbutton.set_value(float(conf[0]))
        if self.config['fan_2'] != '':
            self.spd2 = self.change_speed(int(conf[1]), self.spd2, self.config['fan_2'])
            spinbutton = self.wTree.get_widget('speed_2')
            spinbutton.set_value(float(conf[1]))
        if self.config['fan_3'] != '':
            self.spd3 = self.change_speed(int(conf[2]), self.spd3, self.config['fan_3'])
            spinbutton = self.wTree.get_widget('speed_3')
            spinbutton.set_value(float(conf[2]))
        if self.config['fan_4'] != '':
            self.spd4 = self.change_speed(int(conf[3]), self.spd4, self.config['fan_4'])
            spinbutton = self.wTree.get_widget('speed_4')
            spinbutton.set_value(float(conf[3]))

    ### Change speed of fan ###
    def change_speed(self, spd_new, spd, fan_path):
        # new speed rounding
        if self.config['speed_rounding'] == 1:
            spd_new = int(round(spd_new, -1))

        if spd_new != spd:
            val = str(int(2.55 * spd_new))
            try:
                os.popen('echo ' + val + ' > ' + fan_path)
            except:
                print 'ERROR: Can\'t change value of ' + fan_path
            spd = spd_new
        return spd

    ### Handle speed changes ###
    def spd_1_changed(self, widget):
        if self.config['fan_1'] != '':
            self.spd1 = self.change_speed(widget.get_value_as_int(), self.spd1, self.config['fan_1'])

    def spd_2_changed(self, widget):
        if self.config['fan_2'] != '':
            self.spd2 = self.change_speed(widget.get_value_as_int(), self.spd2, self.config['fan_2'])

    def spd_3_changed(self, widget):
        if self.config['fan_3'] != '':
            self.spd3 = self.change_speed(widget.get_value_as_int(), self.spd3, self.config['fan_3'])

    def spd_4_changed(self, widget):
        if self.config['fan_4'] != '':
            self.spd4 = self.change_speed(widget.get_value_as_int(), self.spd4, self.config['fan_4'])

    ### END APP ###
    def kill(self, widget):
        # Disable manual fan speed
        print 'EXIT'
        enable_pwm('0', self.config['fan_1'])
        enable_pwm('0', self.config['fan_2'])
        enable_pwm('0', self.config['fan_3'])
        enable_pwm('0', self.config['fan_4'])
        gtk.main_quit()

    ### Runs deamon every couple seconds - defined in config ###
    def show(self, widget):
        glib.timeout_add_seconds(self.config['refresh_time'], self.deamon)

    ### Hearth of the app ###
    def deamon(self):
        # Check if the program is still able to control the fan
        if os.popen('cat ' + self.config['fan_1']).read().rstrip() != 1:
            enable_pwm('1', self.config['fan_1'])
            enable_pwm('1', self.config['fan_2'])
            enable_pwm('1', self.config['fan_3'])
            enable_pwm('1', self.config['fan_4'])
		
        # get fan speeds and temperatures
        info = os.popen('sensors').read()
        tmp = info.splitlines()
        for x in range(len(tmp)):
            if self.config['fan_id_1'] in tmp[x]:
                fan_1_spd = tmp[x].split()[self.config['fan_id_1'].count(' ') + 1]
            if self.config['fan_id_2'] in tmp[x]:
                fan_2_spd = tmp[x].split()[self.config['fan_id_2'].count(' ') + 1]
            if self.config['fan_id_3'] in tmp[x]:
                fan_3_spd = tmp[x].split()[self.config['fan_id_3'].count(' ') + 1]
            if self.config['fan_id_4'] in tmp[x]:
                fan_4_spd = tmp[x].split()[self.config['fan_id_4'].count(' ') + 1]
            if self.config['temp_id_1'] in tmp[x]:
                temp_1 = tmp[x].split()[self.config['temp_id_1'].count(' ') + 1]
            if self.config['temp_id_2'] in tmp[x]:
                temp_2 = tmp[x].split()[self.config['temp_id_2'].count(' ') + 1]
            if self.config['temp_id_3'] in tmp[x]:
                temp_3 = tmp[x].split()[self.config['temp_id_3'].count(' ') + 1]
            if self.config['temp_id_4'] in tmp[x]:
                temp_4 = tmp[x].split()[self.config['temp_id_4'].count(' ') + 1]
            if self.config['temp_id_5'] in tmp[x]:
                temp_5 = tmp[x].split()[self.config['temp_id_5'].count(' ') + 1]

        degree = u'\N{DEGREE SIGN}'

        # Code for nvidia with proprietary drivers
        if self.config['temp_gpu'] == 'nvidia-closed':
            info = os.popen('nvidia-settings -q gpucoretemp').read()
            tmp = info.splitlines()
            for line in tmp:
                if 'Attribute \'GPUCoreTemp\'' in line:
                    tgpu = '+' + line.split(': ')[1] + '0' + degree + 'C'
        else:
            tgpu = 'ERROR'

        # Check temps and play sound
        if int(temp_1.split('.')[0]) > self.config['max_temp_1']:
            run_alarm()
        elif int(temp_2.split('.')[0]) > self.config['max_temp_2']:
            run_alarm()
        elif int(temp_3.split('.')[0]) > self.config['max_temp_3']:
            run_alarm()
        elif int(temp_4.split('.')[0]) > self.config['max_temp_4']:
            run_alarm()
        elif int(temp_5.split('.')[0]) > self.config['max_temp_5']:
            run_alarm()
        elif int(tgpu.split('.')[0]) > self.config['max_temp_gpu']:
            run_alarm()

        # show RPM value
        rpm_label = self.wTree.get_widget('rpm_1')
        rpm_label.set_text(fan_1_spd + ' RPM')
        rpm_label = self.wTree.get_widget('rpm_2')
        rpm_label.set_text(fan_2_spd + ' RPM')
        rpm_label = self.wTree.get_widget('rpm_3')
        rpm_label.set_text(fan_3_spd + ' RPM')
        rpm_label = self.wTree.get_widget('rpm_4')
        rpm_label.set_text(fan_4_spd + ' RPM')
        
        # show temp value
        temp_label = self.wTree.get_widget('temp_1')
        temp_label.set_text(temp_1)
        temp_label = self.wTree.get_widget('temp_2')
        temp_label.set_text(temp_2)
        temp_label = self.wTree.get_widget('temp_3')
        temp_label.set_text(temp_3)
        temp_label = self.wTree.get_widget('temp_4')
        temp_label.set_text(temp_4)
        temp_label = self.wTree.get_widget('temp_5')
        temp_label.set_text(temp_5)
        temp_label = self.wTree.get_widget('temp_gpu')
        temp_label.set_text(tgpu)

        return True


if __name__ == '__main__':
    if os.geteuid() != 0:
        exit('You need root privileges to run this program')
    program = main()
    gtk.main()
