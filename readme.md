Speeder
-------
Zdenek Veleba - velezd.tux@gmail.com

To sum this up - program works on my current pc very well and it's great proof of concept. But I will stop the development and focus on rewriting it to GTK 3, with "dynamic" GUI, more scaleable, with proper threading and hopefully easier to setup.

GTK tool for monitoring temperatures of PC components and regulating fan speeds.
Program is still under heavy development, but it's already working.

Tested hardware:
Intel Core i5
Nvidia GTX 770

Tested OS:
Xubuntu 13.04
ubuntu GNOME 14.04

Dependency
----------
lm-sensors - tested versions 3.3.2, 3.3.4

gtk, glib, os - python modules

License
-------
This is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License. If not, see http://www.gnu.org/licenses/.